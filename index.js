const express = require("express");
var cookieParser = require("cookie-parser");
var chalk = require("chalk");

var app = express();

app.use(cookieParser());
app.use(express.urlencoded({ extended: false }));

const db = {
  ali: {
    password: "123",
    token: "",
  },
  mehran: {
    password: "456",
    token: "",
  },
  yasin: {
    password: "789",
    token: "",
  },
};

const pageCreator = (body, style = ``) => `
<html>
  <head>
    <title>ali asgary</title>
    <style>
      html {
        background-color: black;
        color: yellow;
      }
      a {
        color: blue;
      }
      ${style}
    </style>
  </head>
  <body>
    <h1>In The Name Of GOD</h1>
    <a href="/">home</a>
    <a href="/skills">skills</a>
    <a href="/bio">bio</a>
    <a href="/login">login</a>
    <a href="/secret">secret</a>
    <a href="/secret2">secret2</a>
    ${body}
  </body>
</html>
`;

const htmlForRoot = pageCreator(`
  <h3>Website for introducing Ali Asgary</h3
`);

const htmlForSkills = pageCreator(`
<ol>
  <li>management</li>
  <li>javascript</li>
  <li>expresss.js</li>
  <li>...</li>
</ol>
`);

const htmlForBio = pageCreator(`
  <div>25 years old from Tehran<div>
`);

const htmlForLogin = pageCreator(`
  <form action="/login" method="POST">
      <input name="username" placeholder="username"></input>
      <input name="password" type="password" placeholder="password"></input>
      <input type="submit" value="login">
  </form>
`);

const htmlForSecret = pageCreator(
  `<div>i study material science at sharif university</div>`
);
const htmlForSecret2 = pageCreator(`<div>Secret2</div>`);

const htmlFor404 = pageCreator(
  `
  <div>404: page does not exist<div>
`,
  `
  div {
    color: red;
  }
`
);

const login = (req, res, next) => {
  if (
    req.username &&
    req.token &&
    db[req.username] &&
    db[req.username].token === Number(req.token)
  ) {
    next();
  } else {
    res.redirect("/login");
  }
};

app.use((req, res, next) => {
  console.log(
    chalk.yellow(new Date()),
    chalk.blue(req.method),
    chalk.red(req.url)
  );

  next();
});

app.use((req, res, next) => {
  req.token = req.cookies.token;
  req.username = req.cookies.username;
  next();
});

app.get("/", (req, res) => {
  res.send(htmlForRoot);
});

app.get("/skills", (req, res) => {
  res.send(htmlForSkills);
});

app.get("/bio", login, (req, res) => {
  res.send(htmlForBio);
});

app.get("/login", (req, res) => {
  res.send(htmlForLogin);
});

app.post("/login", (req, res) => {
  const { username, password } = req.body;

  if (db[username] && db[username].password === password) {
    const token = Math.floor(Math.random() * 100000000);
    db[username].token = token;
    res.cookie("token", token);
    res.cookie("username", username);
    res.redirect("/secret");
  } else {
    res.redirect("/login");
  }
});

app.get("/secret", login, (req, res) => {
  res.send(htmlForSecret);
});

app.get("/secret2", login, (req, res) => {
  res.send(htmlForSecret2);
});

app.get("*", (req, res) => {
  res.send(htmlFor404);
});

app.listen(8000);
